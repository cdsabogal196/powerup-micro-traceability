package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RegistroEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TrazabilityHandlerRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.ITraceabilityHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/traceability")
@RequiredArgsConstructor
public class TraceabilityRestController {

    @Autowired
    private ITraceabilityHandler traceabilityHandler;


    @PostMapping
    public ResponseEntity<RegistroEntity> generateTraceability(@RequestBody TrazabilityHandlerRequestDTO pedido) {
        RegistroEntity registro = traceabilityHandler.generateTrazabilidad(pedido);
        return new ResponseEntity<>(registro, HttpStatus.CREATED);
    }


    @GetMapping("/consultar")
    public List<RegistroEntity> consultOrderTraceability(@RequestParam("idCliente") long idCliente, @RequestParam("mail") String mail, @RequestParam("password") String password) throws Exception {

        return traceabilityHandler.ConsultOrdeTraceability(idCliente, mail, password);
    }

    @GetMapping("/fechas-registros/{idPedido}")
    public ResponseEntity<Map<String, Object>> getRegistersDates(@PathVariable Long idPedido) {
        try {
            return traceabilityHandler.obtenerFechasRegistros(idPedido);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/ranking")
    public ResponseEntity<List<Map<String, Object>>> getFastestOrders() {

        List<Map<String, Object>> ranking = traceabilityHandler.generateRanking();

        return ResponseEntity.ok(ranking);
    }

}
