package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.Gson;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RegistroEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TrazabilityHandlerRequestDTO;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.ITraceabilityHandler;
import com.pragma.powerup.usermicroservice.domain.model.OrderStateEnum;
import com.pragma.powerup.usermicroservice.domain.model.RoleEnum;
import com.pragma.powerup.usermicroservice.domain.repositories.ITraceabilityRepository;
import lombok.RequiredArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class TraceabilityHandlerImpl implements ITraceabilityHandler {

    @Autowired
    private ITraceabilityRepository traceabilityRepository;

    public String getRole(String mail, String password) {
        HttpClient client = HttpClient.newHttpClient();

        String requestBody = "{\"mail\":\"" + mail + "\", \"password\":\"" + password + "\"}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/auth/login"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();
        HttpResponse<String> response;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        String responseBody = response.body();

        Gson gson = new Gson();
        JwtResponseDto responseObject = gson.fromJson(responseBody, JwtResponseDto.class);
        String token = responseObject.getToken();

        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();

        String payload = new String(decoder.decode(chunks[1]));
        JwtResponseDto jwtResponseDto = gson.fromJson(payload, JwtResponseDto.class);

        return jwtResponseDto.getRoles().get(0);

    }

    public RegistroEntity generateTrazabilidad(TrazabilityHandlerRequestDTO order) {
        try {

            List<RegistroEntity> stateOrders = traceabilityRepository.findByIdPedidoAndEstadoNuevo(order.getIdOrder(), order.getNewState());

            if (stateOrders.size() > 0) {
                throw new Exception("The order with id " + order.getIdOrder() + " with state " + order.getNewState() + " already exist.");
            }

            LocalDateTime actualDate = LocalDateTime.now();
            RegistroEntity register = new RegistroEntity();
            register.setIdPedido(order.getIdOrder());
            register.setIdCliente(order.getIdClient());
            register.setCorreoCliente(order.getMailClient());
            register.setFecha(actualDate);
            register.setEstadoAnterior(order.getPreviousState());
            register.setEstadoNuevo(order.getNewState());
            register.setIdEmpleado(order.getIdEmployee());
            register.setCorreoEmpleado(order.getMailEmployee());

            return traceabilityRepository.save(register);
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }
    }

    public List<RegistroEntity> ConsultOrdeTraceability(long idCliente, String mail, String password) throws Exception {
        try {
            String role = getRole(mail, password);
            if (!role.equals(RoleEnum.ROLE_CLIENT.toString())) {
                throw new Exception("The user is not a client. Please try again with a different user.");
            }

            return traceabilityRepository.findByIdPedido(idCliente);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Something was wrong trying to get traceability of the order, details: " + e.getMessage());
        }
    }

    public RegistroEntity findStartPendingOrder(long idPendiente) {
        try {
            return traceabilityRepository.findFechaByIdPedidoAndEstadoNuevoPendiente(idPendiente);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public RegistroEntity findEndReadyOrder(long idPendiente) {
        try {
            return traceabilityRepository.findFechaByIdPedidoAndEstadoNuevoListo(idPendiente);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("The id of the pending order doesn't exist or is invalid, details: ", e);
        }
    }

    public List<RegistroEntity> findAllEndReadyOrders() {
        try {
            return traceabilityRepository.findByEstadoNuevo(OrderStateEnum.Listo.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public String findIdEmployeeByIdPedido(long idPedido) {
        String resultadoConsulta = traceabilityRepository.findIdEmpleadoByIdPedido(idPedido);
        JSONObject jsonObject;
        int idEmpleado = -1;

        try {
            jsonObject = new JSONObject(resultadoConsulta);
            idEmpleado = jsonObject.getInt("idEmpleado");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return String.valueOf(idEmpleado);
    }

    @Override
    public ResponseEntity<Map<String, Object>> obtenerFechasRegistros(Long idPedido) {
        RegistroEntity pendingOrder = traceabilityRepository.findFechaByIdPedidoAndEstadoNuevoPendiente(idPedido);
        RegistroEntity readyOrder = traceabilityRepository.findFechaByIdPedidoAndEstadoNuevoListo(idPedido);

        Map<String, Object> registersDates = new HashMap<>();

        LocalDateTime pendingOrderDate = pendingOrder.getFecha();
        LocalDateTime readyOrderDate = readyOrder.getFecha();
        Duration duration = Duration.between(pendingOrderDate, readyOrderDate);

        registersDates.put("registroPendiente", pendingOrderDate);
        registersDates.put("registroListo", readyOrderDate);
        registersDates.put("duracion", String.format("%02d:%02d:%02d", duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart()));

        return ResponseEntity.ok(registersDates);
    }

    @Override
    public List<Map<String, Object>> generateRanking() {
        List<RegistroEntity> readyOrders = findAllEndReadyOrders();
        List<Map<String, Object>> ranking = new ArrayList<>();

        for (RegistroEntity readyOrder : readyOrders) {
            long idOrder = readyOrder.getIdPedido();
            RegistroEntity pendingOrder = findStartPendingOrder(idOrder);
            String idEmployee = findIdEmployeeByIdPedido(idOrder);

            if (pendingOrder != null) {
                LocalDateTime pendingOrderDate = pendingOrder.getFecha();
                LocalDateTime readyOrderDate = readyOrder.getFecha();
                Duration duration = Duration.between(pendingOrderDate, readyOrderDate);

                Map<String, Object> registroInfo = new HashMap<>();
                registroInfo.put("idPedido", idOrder);
                registroInfo.put("duracion", String.format("%02d:%02d:%02d", duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart()));
                registroInfo.put("idEmpleado", idEmployee);

                ranking.add(registroInfo);
            }
        }
        ranking.sort(Comparator.comparing(registro -> registro.get("duracion").toString()));

        return ranking;
    }


}




