package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class TrazabilityHandlerRequestDTO {
    private Long idOrder;
    private String idClient;
    private String mailClient;
    private String previousState;
    private String newState;
    private Long idEmployee;
    private String mailEmployee;
}
