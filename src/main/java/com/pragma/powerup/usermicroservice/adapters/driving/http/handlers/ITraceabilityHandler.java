
package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;


import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RegistroEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TrazabilityHandlerRequestDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface ITraceabilityHandler {

    RegistroEntity generateTrazabilidad(TrazabilityHandlerRequestDTO pedido);
    List<RegistroEntity> ConsultOrdeTraceability(long idPedido, String mail,String password) throws Exception;
    RegistroEntity findStartPendingOrder(long idCliente);
    RegistroEntity findEndReadyOrder(long idCliente);
    List<RegistroEntity> findAllEndReadyOrders();
    String findIdEmployeeByIdPedido(long idPedido);
    ResponseEntity<Map<String, Object>> obtenerFechasRegistros(Long idPedido);

    List<Map<String, Object>> generateRanking();
}
