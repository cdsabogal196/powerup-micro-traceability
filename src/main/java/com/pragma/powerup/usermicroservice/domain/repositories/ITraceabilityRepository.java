package com.pragma.powerup.usermicroservice.domain.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RegistroEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface ITraceabilityRepository extends MongoRepository<RegistroEntity, Long> {
    @Query("{ 'idPedido' : ?0 }")
    List<RegistroEntity> findByIdPedido(long idCliente);

    @Query(value = "{ idPedido :?0, estadoNuevo : 'Pendiente' }", fields = "{ 'fecha' : 1 }")
    RegistroEntity findFechaByIdPedidoAndEstadoNuevoPendiente(long idPedido);

    @Query(value = "{ idPedido : ?0, estadoNuevo : 'Listo' }", fields = "{ 'fecha' : 1 }")
    RegistroEntity findFechaByIdPedidoAndEstadoNuevoListo(long idPedido);
    @Query("{ 'estadoNuevo' : ?0 }")
    List<RegistroEntity> findByEstadoNuevo(String estadoNuevo);

    @Query ("{ idPedido : ?0 , estadoNuevo : '?1'}")
    List<RegistroEntity> findByIdPedidoAndEstadoNuevo(long idPedido, String estadoNuevo);
    @Query(value = "{ idPedido : ?0 , estadoNuevo : 'Listo'}", fields = "{ 'idEmpleado' : 1 }")
    String findIdEmpleadoByIdPedido(long idPedido);
}