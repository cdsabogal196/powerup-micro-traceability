<br />
<div align="center">
<h3 align="center">PRAGMA POWER-UP</h3>
  <p align="center">
    In this challenge you are going to design the backend of a system that centralizes the services and orders of a restaurant chain that has different branches in the city.
  </p>
</div>

### Built With

* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

* JDK 17 [https://jdk.java.net/java-se-ri/17](https://jdk.java.net/java-se-ri/17)
* Gradle [https://gradle.org/install/](https://gradle.org/install/)
* MySQL [https://dev.mysql.com/downloads/installer/](https://dev.mysql.com/downloads/installer/)

### Recommended Tools
* IntelliJ Community [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)
* Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

### Installation

1. Clone the repository
2. Change directory
   ```sh
   cd power-up-arquetipo-v3
   ```
3. Create a new database in MySQL called powerup
4. Update the database connection settings
   ```yml
   # src/main/resources/application-dev.yml
   spring:
      datasource:
          url: jdbc:mysql://localhost/powerup
          username: root
          password: <your-password>
   ```
5. After the tables are created execute src/main/resources/data.sql content to populate the database
6. Open Swagger UI and search the /auth/login endpoint and login with userDni: 123, password: 1234

<!-- USAGE -->
## Usage

1. Right-click the class PowerUpApplication and choose Run
2. Open [http://localhost:8091/swagger-ui/index.html](http://localhost:80901/swagger-ui/index.html) in your web browser

## Tests

- Right-click the test folder and choose Run tests with coverage
### USE ENPOINT /cellphone/

This is the README file for the project. Below, you will find instructions on how to use the API and access the endpoints using Swagger.

## Step 1: Accessing the API

Once you are in the environment where the API is located, you can access it using the corresponding URL.

## Step 2: Accessing Swagger

To interact with the API, go to the Swagger link. From there, you will be able to see all the available endpoints and test them.

http://localhost:8091/swagger-ui/index.html
# Restaurante

Este proyecto tiene como objetivo proporcionar funcionalidades para clientes y propietarios de un restaurante, permitiendo consultar la trazabilidad de los pedidos, cancelar pedidos y obtener información sobre la eficiencia de los mismos.

### User Story

## User Story 17: Client - Consult order traceability /traceability/fechas-registros/{orderID}

This API endpoint allows clients to retrieve the traceability information for a specific order by its ID.

# Requirements:

- A record must be created for each change in the order's status.
- Each client can only consult the history of their own orders.
- A record must be created for each change in the order's status.
Each client can only consult the history of their own orders.

```
GET /traceability/fechas-registros/{orderID}
```

# Request Parameters

1. {orderID}: The ID of the order for which the traceability information is requested. Replace {orderID} with the actual ID of the order.
2. As a restaurant client, I need to be able to consult the traceability of my orders to see the speed of the requested service.



### User Story 18: Owner - Consult order efficiency /traceability/ranking
As the owner of a restaurant, I need to be able to know the efficiency of orders to provide a better experience for customers.

# Requirements:

1. Show the elapsed time between the start and completion of each order.
2. Show a ranking of employees based on the average time between the start and completion of orders.
# Endpoints
The main endpoints of the project are detailed below:

```
POST /traceability/generate
```

# MongoDB Installation:
Download and install MongoDB on your system following the instructions provided by MongoDB on their official website.

# Prerequisites - MongoDB Configuration:
- Start the MongoDB server on your machine.
- Define the connection configuration, such as IP address, port, and necessary access credentials to connect to the database from your application.
- Create a database and a collection.
- Design the necessary data schema to store information about orders and state change records. This may include fields such as order ID, current status, date and time of the state change, among others.

Connect and use MongoDB in your application:
1. Configure the connection to the MongoDB database in your application using a MongoDB-compatible driver or library.
2. Ensure that each client can only query the state change records associated with their own orders by applying appropriate access restrictions in your database queries.
3. This endpoint allows generating the traceability of an order. You should send a POST request with the following body:

### Steps to set up the prerequisite:
An endpoint was created to save the traceability in the mongo db database

``` GET /traceability/fechas-registros/12345 ```

```json
{
  "idOrder": 0,
  "idClient": "string",
  "mailClient": "string",
  "previousState": "string",
  "newState": "string",
  "idEmployee": 0,
  "mailEmployee": "string"
}
  ```

1. idOrder: ID of the order referenced by the state change record.
2. idClient: ID of the client associated with the order.
3. mailClient: Email of the client associated with the order.
4. previousState: Previous state of the order.
5. newState: New state of the order.
6. idEmployee: ID of the employee responsible for the state change.
7. mailEmployee: Email of the employee responsible for the state change.

## Microservice Usage
This microservice is used by the Plazoleta project to perform the following user stories:
1. H 11: Place an order
When placing an order, the corresponding state change is recorded.

POST ` /pedido`


2. HU 13: Assign to an order and change status to "in preparation"
When an employee is assigned to an order, the status change to "in preparation" is recorded.

PUT ` /pedido/cambiarEstadoPreparacion`

3. HU 14: Notify that the order is ready
Marking an order as "done" records the corresponding status change.
` http://localhost:8095/swagger-ui/index.html#/order-messaging-rest-controller/callNotification/cellphone/customer-notification`


4. HU 15: Deliver order (Mark it as delivered)
Marking an order as "delivered" records the corresponding status change.
POST
` /pedido/{idPedido}/entregar/{pinSeguridad}`



All order status change records are stored in a database for later consultation. Make sure you have a database configured and set up correctly for the proper functioning of the microservice. mongo database

### Order Traceability API - Get Order Traceability by ID
This API endpoint allows clients to retrieve the traceability information for a specific order by its ID.

```
GET /traceability/fechas-registros/{orderID}
```

- {orderID}: The ID of the order for which the traceability information is requested. Replace {orderID} with the actual ID of the order.
# Response
- The API response will contain the traceability information of the specified order in JSON format. The response will include the following fields:

- orderID: The ID of the order.
- orderStatus: The current status of the order.
- traceabilityRecords: An array of traceability records representing the history of state changes for the order. Each record contains the following fields:
- timestamp: The date and time when the state change occurred.
- previousStatus: The previous status of the order.
- newStatus: The new status of the order.


<br />
